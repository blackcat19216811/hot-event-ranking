package com.example.springbootvue.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.springbootvue.dao.CommentaryMapper;
import com.example.springbootvue.entity.Comment;
import com.example.springbootvue.entity.Commentary;
import com.example.springbootvue.service.CommentaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
@RestController
@RequestMapping("/commentary")
public class CommentaryController {
    @Autowired
    private CommentaryService commentaryService;
    @Autowired
    private CommentaryMapper commentaryMapper;


    //    评论
    @PostMapping("/list")
    public List<Commentary> mlist(@RequestParam String id){
        QueryWrapper<Commentary> fuzz = new QueryWrapper<>();
        fuzz.like("cid",id);
        List<Commentary> list = commentaryMapper.selectList(fuzz);
        System.out.println(list);
        return list;
    }


//    提交评论内容
//    提交两个值，第一个值是cid 第二个值是评论内容
    @PostMapping("/GetAllinput")
    public int GetAllInput(@RequestParam String AllInput,@RequestParam String cid){
//        获取后添加到数据表
        System.out.println("GetAllinput：" + AllInput + cid);
        Commentary newComment = new Commentary();
        newComment.setCid(Integer.parseInt(cid));
        newComment.setCommentarycontent(AllInput);
        // 获取当前日期
        LocalDate currentDate = LocalDate.now();
        System.out.println("当前日期: " + currentDate);
        // 创建一个格式化器来按照"yyyy-MM-dd"的格式进行格式化
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = currentDate.format(formatter);
        System.out.println("格式化后的日期: " + formattedDate);
        // 将格式化后的日期字符串转换为 Date 类型
        LocalDate parsedDate = LocalDate.parse(formattedDate, formatter);
        Date date = java.sql.Date.valueOf(parsedDate);
        newComment.setCreationtime(date); // 设置评论的创建时间

        // 将评论插入到数据库中
        commentaryMapper.insert(newComment);
        return 1;
    }

}

