package com.example.springbootvue.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.springbootvue.dao.ChartsMapper;
import com.example.springbootvue.dao.CommentMapper;
import com.example.springbootvue.dao.CommentaryMapper;
import com.example.springbootvue.entity.Charts;
import com.example.springbootvue.entity.Comment;
import com.example.springbootvue.entity.Commentary;
import com.example.springbootvue.service.ChartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
@RestController
@RequestMapping("/charts")
public class ChartsController {
//    新闻列表

    @Autowired
    private ChartsService chartsService;
    @Autowired
    private ChartsMapper chartsMapper;

    @Autowired
    private CommentMapper commentMapper;

    @PostMapping("list")
    public Page<Charts> findlist(@RequestBody Page<Charts> page) {
        System.out.println("分页控制器就收参数:" + page);
        Page<Charts> p = chartsService.page(page);
        System.out.println(p);
        return p;
    }

    @GetMapping("/mlist")
    public List<Charts> mlist(@RequestParam String input) {
        QueryWrapper<Charts> fuzz = new QueryWrapper<>();
        fuzz.like("keywords", input);
        List<Charts> charts = chartsMapper.selectList(fuzz);
        System.out.println(charts);
        return charts;
    }

    public int getTableCount() {
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        return commentMapper.selectCount(queryWrapper);
    }


    @PostMapping("/AddList")
    public int AddList(@RequestParam String name, @RequestParam String desc) {
        System.out.println("获取AddList:" + name + desc);
        Charts charts = new Charts();
        charts.setKeywords(name);
        // 获取当前日期
        LocalDate currentDate = LocalDate.now();
        System.out.println("当前日期: " + currentDate);
        // 创建一个格式化器来按照"yyyy-MM-dd"的格式进行格式化
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = currentDate.format(formatter);
        System.out.println("格式化后的日期: " + formattedDate);
        charts.setCreationtime(formattedDate);
//        随机生成指数 start
        Random random = new Random();
        int min = 10000; // 最小的五位数
        int max = 99999; // 最大的五位数
        int randomNumber = random.nextInt(max - min + 1) + min;
//        随机生成指数 end
        charts.setIndexnum(Integer.toString(randomNumber));
        chartsMapper.insert(charts);

        Comment comment = new Comment();
        comment.setNews(desc);
        comment.setCreationtime(formattedDate);
        comment.setCommentID(getTableCount() + 1);
        int insert = commentMapper.insert(comment);


        return insert;
    }

    @PostMapping("/DelList")
    public int DelList(@RequestParam String id) {
        System.out.println("delId捕获:" + id);
        chartsMapper.deleteById(id);
        int id1 = commentMapper.deleteById(id);
//        评论不删除 数据可以用
//        为了确保数据一致性
//        存在Bug，如果鼠标滑动过快，会造成信息紊乱
        return id1;
    }

    //    更新
    @PostMapping("/upDateList")
    public int upDateList(@RequestParam String name, @RequestParam String desc,@RequestParam String id) {
        System.out.println("获取upDateList:" + name + desc);
//      MyBatis plus UpDate
        Charts charts = new Charts();
        UpdateWrapper<Charts> chartsUpdateWrapper = new UpdateWrapper<>();
        chartsUpdateWrapper.eq("id",id);
        charts.setKeywords(name);
        int update = chartsMapper.update(charts, chartsUpdateWrapper);

        Comment comment = new Comment();
        UpdateWrapper<Comment> commentUpdateWrapper = new UpdateWrapper<>();
        comment.setNews(desc);
        commentUpdateWrapper.eq("id",id);
        int update1 = commentMapper.update(comment, commentUpdateWrapper);
        System.out.println("update1:" + update1);
        return update;
    }

}

