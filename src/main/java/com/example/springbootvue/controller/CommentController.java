package com.example.springbootvue.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.springbootvue.dao.CommentMapper;
import com.example.springbootvue.entity.Charts;
import com.example.springbootvue.entity.Comment;
import com.example.springbootvue.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
@RestController
@RequestMapping("/comment")
public class CommentController {
//    新闻内容
    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentMapper commentMapper;

    @GetMapping("/list")
    public List<Comment> mlist(@RequestParam String id){
        QueryWrapper<Comment> fuzz = new QueryWrapper<>();
        fuzz.like("id",id);
        List<Comment> list = commentMapper.selectList(fuzz);
        System.out.println(list);
        return list;
    }

}

