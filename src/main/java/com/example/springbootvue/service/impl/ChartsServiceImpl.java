package com.example.springbootvue.service.impl;

import com.example.springbootvue.entity.Charts;
import com.example.springbootvue.dao.ChartsMapper;
import com.example.springbootvue.service.ChartsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
@Service
public class ChartsServiceImpl extends ServiceImpl<ChartsMapper, Charts> implements ChartsService {

}
