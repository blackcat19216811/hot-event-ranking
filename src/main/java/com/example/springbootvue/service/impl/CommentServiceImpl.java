package com.example.springbootvue.service.impl;

import com.example.springbootvue.entity.Comment;
import com.example.springbootvue.dao.CommentMapper;
import com.example.springbootvue.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

}
