package com.example.springbootvue.service.impl;

import com.example.springbootvue.entity.Commentary;
import com.example.springbootvue.dao.CommentaryMapper;
import com.example.springbootvue.service.CommentaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
@Service
public class CommentaryServiceImpl extends ServiceImpl<CommentaryMapper, Commentary> implements CommentaryService {



}
