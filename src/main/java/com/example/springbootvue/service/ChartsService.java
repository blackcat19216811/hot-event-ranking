package com.example.springbootvue.service;

import com.example.springbootvue.entity.Charts;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
public interface ChartsService extends IService<Charts> {

}
