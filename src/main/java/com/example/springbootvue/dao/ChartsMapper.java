package com.example.springbootvue.dao;

import com.example.springbootvue.entity.Charts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
public interface ChartsMapper extends BaseMapper<Charts> {

}
