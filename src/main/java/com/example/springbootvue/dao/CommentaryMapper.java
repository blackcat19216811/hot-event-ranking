package com.example.springbootvue.dao;

import com.example.springbootvue.entity.Commentary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author frr
 * @since 2023-12-13
 */
public interface CommentaryMapper extends BaseMapper<Commentary> {

}
