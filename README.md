# HotEventRanking
<img alt="Static Badge" src="https://img.shields.io/badge/SpringBoot_-5.2.1.RELEASE-blue">
<img alt="Static Badge" src="https://img.shields.io/badge/Spring_-5.2.1.RELEASE-blue">
<img alt="Static Badge" src="https://img.shields.io/badge/Element_UI-blue">
<img alt="Static Badge" src="https://img.shields.io/badge/Mybatis_plus-black">
<img alt="Static Badge" src="https://img.shields.io/badge/Vue2.js-dark_green?style=%20flat-square">
<img alt="Static Badge" src="https://img.shields.io/badge/MySql-blue">
<img alt="Static Badge" src="https://img.shields.io/badge/maven-blue">
<img alt="Static Badge" src="https://img.shields.io/badge/Navicat%20Premium%2016%20-gold">

#### 介绍
热点时间排行榜 前后端 包含增删改查
评论 获取新闻内容

#### 软件架构
使用前端:bootstrap+Vue 后端:Spring+mybatis+maven


#### 安装教程
克隆本项目到本地
部署数据库
测试链接




